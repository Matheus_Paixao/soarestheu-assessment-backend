<?php
// Arquivo de conexao
require_once("../includes/conexao.php");
// Arquivo da classe categoria
require_once("../classes/Categoria.php");

// Instanciando objetos
$Categoria = new Categoria();

$nomeCategoria      =   $_POST['category-name'];
$codigoCategoria    =   $_POST['category-code'];

$status = $Categoria->CadCategoria($nomeCategoria, $codigoCategoria);

if ($status) {
    $msg = "Categoria cadastrado com sucesso!";
    $link = "window.location.href='../?page=cadastraCategoria'";
} else {
    $msg = "Erro ao cadastrar categoria!";
    $link = "window.history.back()";
}

echo "<script>alert('$msg'); $link;</script>";

