<?php

// Arquivo de conexao
require_once("../includes/conexao.php");
// Arquivo da classe categoria
require_once("../classes/Categoria.php");

// Instanciando objetos
$Categoria    = new Categoria();

$idCat = $_GET['id'];

if ($Categoria->ApagaCategoria($idCat)) {
    $msg = "Categoria deletada com sucesso!";
    $link = "window.location.href='../?page=categoria'";
} else {
    $msg = "Erro ao deletar categoria!";
    $link = "window.history.back()";
}

echo "<script>alert('$msg');$link;</script>";
