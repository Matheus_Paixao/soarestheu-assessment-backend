<?php
// Arquivo de conexao
require_once("../includes/conexao.php");
// Arquivo da classe Produto
require_once("../classes/Produto.php");

// Instanciando objetos
$Produto    = new Produto();

$idProd = $_GET['id'];

if ($Produto->ApagaProduto($idProd)) {
    $msg = "Produto deletado com sucesso!";
    $link = "window.location.href='../?page=produto'";
} else {
    $msg = "Erro ao deletar produto!";
    $link = "window.history.back()";
}

echo "<script>alert('$msg');$link;</script>";
