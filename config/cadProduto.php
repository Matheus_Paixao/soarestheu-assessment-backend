<?php

// Arquivo de conexao
require_once("../includes/conexao.php");
// Arquivos de classes
require_once("../classes/Produto.php");
require_once("../classes/Catalogar.php");

// Instanciando objetoss
$Produto    = new Produto();
$Catalogar  = new Catalogar();

$nomeProd       = $_POST['nome'];
$skuProd        = $_POST['sku'];
$preco          = $_POST['preco'];
$quantidade     = $_POST['qnt'];
$categoria      = $_POST['categoria'];
$descricao      = $_POST['descricao'];

$imagem         = $_FILES['imagemProduto'];
$dir            = "../assets/images/product/";
if ($imagem['name']) {
    if (!move_uploaded_file($imagem['tmp_name'], $dir.$imagem['name'])) {
        echo "<script>alert('Erro no upload da imagem!');window.history.back();</script>";
        exit;
    }
}

$idProd = $Produto->CadProduto($nomeProd, $skuProd, $preco, $descricao, $quantidade, $imagem['name']);

if ($idProd && ($Catalogar->CatalogaProdutos($idProd, $categoria))) {
    $msg = "Produto cadastrado com sucesso!";
    $link = "window.location.href='../?page=cadastraProduto'";
} else {
    $msg = "Erro ao cadastrar Produto!";
    $link = "window.history.back()";
}

echo "<script>alert('$msg');$link;</script>";

