<?php

require_once("includes/conexao.php");

$conexao = new MySQLi (_SERV,_USER,_PSW,_BD) or die("Erro ao conectar com o banco de dados.");
$conexao->set_charset("utf8");
$rodou=0;
$sql = $conexao->prepare("CREATE TABLE IF NOT EXISTS `categoria` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `nome` varchar(250) NOT NULL DEFAULT '0',
    `cod` varchar(250) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;");
if($sql->execute()){
    $rodou++;
}
$sql->close();


$sql = $conexao->prepare("CREATE TABLE IF NOT EXISTS `log` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `acao` varchar(500) NOT NULL DEFAULT '0',
    `descricao` varchar(500) NOT NULL DEFAULT '0',
    `dataAlteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;");
if($sql->execute()){
    $rodou++;
}
$sql->close();

$sql = $conexao->prepare("CREATE TABLE IF NOT EXISTS `produto` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `nome` varchar(250) NOT NULL DEFAULT '0',
    `cod` int(11) NOT NULL DEFAULT '0',
    `preco` decimal(17,2) NOT NULL DEFAULT '0.00',
    `descricao` varchar(750) DEFAULT '0.00',
    `quantidade` int(11) NOT NULL DEFAULT '0',
    `imagem` varchar(250) DEFAULT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;");
if($sql->execute()){
    $rodou++;
}
$sql->close();

$sql = $conexao->prepare("CREATE TABLE IF NOT EXISTS `prodcategoria` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `idProduto` int(11) NOT NULL DEFAULT '0',
    `idCategoria` int(11) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY `FK_prodcategoria_categoria` (`idCategoria`),
    KEY `FK_prodcategoria_produto` (`idProduto`),
    CONSTRAINT `FK_prodcategoria_categoria` FOREIGN KEY (`idCategoria`) REFERENCES `categoria` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
    CONSTRAINT `FK_prodcategoria_produto` FOREIGN KEY (`idProduto`) REFERENCES `produto` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
  ) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1 COMMENT='Tabela que vai fazer a relação entre produto e categoria.';");
if($sql->execute()){
    $rodou++;
}else{
    echo $sql->error;exit;
}
$sql->close();

if($rodou == 4){
    echo "<script>alert('Tabelas criadas no banco de dados');window.location.href='./?page=inicio';</script>";
}
