#Desafio Web Jump
- Desenvolvido por: Matheus F. de A. Soares

#Configurações básicas
- MariaDB/MySQL
- PHP 5.5+

#Iniciando o sistema
- Abra o arquivo de conexao (conexao.php) existente na pasta "includes" e altere as constantes lá existentes, informando os dados solicitados (Servidor,Usuário,Senha e Banco de dados).
- Feito essa alteração, execute o arquivo *BancoDeDados.php*, ele irá criar as tabelas necessárias para o sistema funcionar.
- Após ter feito seguido os passos anteriores, o sistema está pronto para ser executado no seu servidor.

#Telas disponiveis
- Cadastro de Produtos: Na tela está disponivel para cadastro do produto as seguintes informações: SKU, nome, imagem, preço, quantidade, categorias e descrição.
- Cadastro de categorias: Na tela está disponivel para cadastro da categoria as seguintes informações: Nome e código.
- Categoria: Na tela está disponivel as informações cadastradas sobre categoria.
- Produtos: Na tela está disponivel as informações cadastradas sobre produtos.
- Importação de Arquivo: Na tela está um campo para que seja anexado uma planilha com os dados a serem importados.
- Logs: Na tela está disponível 