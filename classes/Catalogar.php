<?php
require_once("Log.php");
class Catalogar extends Logs
{

    private $conexao;

    ######### CONSTRUTOR:
    public function __construct()
    {
        $this->conexao = new MySQLi (_SERV,_USER,_PSW,_BD) or die("Erro ao conectar com o banco de dados.");
        $this->conexao->set_charset("utf8");
            
        parent::__construct();
    }

    // Cria o vinculo das categorias com os produtos
    public function CatalogaProdutos($idProd, $idCat)
    {
        for( $i = 0; $i < count($idCat); $i++ ) {
            
            $sql = $this->conexao->prepare("INSERT INTO prodcategoria (idProduto,idCategoria) VALUES (?,?)");
            $sql->bind_param('dd', $idProd, $idCat[$i]);
            if( !$sql->execute() ){
                return(0);
            }
            $sql->close();
            $this->CadLog("Adicionou o produto ".$idProd." a uma nova categoria ->".$idCat[$i],"Usuário realizou cadastro de um novo produto em uma nova categoria");
        }
        
        return(1);
    }

    // Pega o nome da categoria de acordo com o produto:
    public function GetCategoriaProd($id)
    {
        $sql = $this->conexao->prepare("SELECT c.nome FROM categoria c, prodcategoria pc
                                        WHERE pc.idCategoria = c.id AND pc.idProduto = ?");
        $sql->bind_param('d', $id);
        $sql->execute();
        $sql->bind_result($nomeCat_);
        while( $sql->fetch() ) {
            $dados[]    =   $nomeCat_;
        }
        $sql->close();
        return($dados);
    }

    // Pega o id das categorias de acordo com o id de cada produto
    public function GetCatProdID($idProd)
    {
        $sql = $this->conexao->prepare("SELECT idCategoria FROM prodcategoria pc WHERE pc.idProduto = ?");
        $sql->bind_param('d', $idProd);
        $sql->execute();
        $sql->bind_result($id_);
        while( $sql->fetch() ) {
            $dados[]    =   $id_;
        }
        $sql->close();
        return($dados);
    }

    // Edita a relação entre a categoria e o produto
    public function EditaCatProd($idProd, $idCat)
    {
        $deleta = $this->ApagaCategoriaProd($idProd);
        if ($deleta) {
            if ($this->CatalogaProdutos($idProd, $idCat)) {
                return(1);
            } else {
                return(0);
            }
        } else {
            return(0);
        }
    }

    // Apaga a relação existente entre a categoria e o produto
    public function ApagaCategoriaProd($idProd)
    {
        $sql = $this->conexao->prepare("DELETE FROM prodcategoria WHERE idProduto = ?");
        $sql->bind_param('d',$idProd);
        if ($sql->execute()) {
            return(1);
        } else {
            return(0);
        }
        $sql->close();
    }
}
