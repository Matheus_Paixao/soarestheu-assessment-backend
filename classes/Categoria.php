<?php 
require_once("Log.php");
class Categoria extends Logs
{

    private $conexao;

    ######### CONSTRUTOR:
    public function __construct(){
        $this->conexao = new MySQLi (_SERV,_USER,_PSW,_BD) or die("Erro ao conectar com o banco de dados.");
        $this->conexao->set_charset("utf8");
        
        parent::__construct();
    }

    // CADASTRO DE CATEGORIA:
    public function CadCategoria($nome, $cod)
    {
        $sql    =   $this->conexao->prepare("INSERT INTO categoria (nome,cod) VALUES (?,?)");
        $sql->bind_param('ss', $nome, $cod);
        if ($sql->execute()) {
            $this->CadLog("Criou uma nova categoria -> ".$nome,"Usuário realizou cadastro de uma nova categoria");
            return($sql->insert_id);
        } else {
            return(0);
        }
    }

    // Pega todas as categorias cadastradas:
    public function GetCategoria()
    {
        $sql = $this->conexao->prepare("SELECT id, nome, cod FROM categoria");
        $sql->execute();
        $sql->bind_result($id_, $nome_, $cod_);
        while ($sql->fetch()) {
            $dados['id'][]    =   $id_;
            $dados['nome'][]  =   $nome_;
            $dados['cod'][]   =   $cod_;
        }
        $sql->close();
        return($dados);
    }

    // Pega a categoria por id
    public function GetCategoriaID($idCat)
    {
        $sql = $this->conexao->prepare("SELECT id, nome, cod FROM categoria WHERE id=?");
        $sql->bind_param('d',$idCat);
        $sql->execute();
        $sql->bind_result($id_, $nome_, $cod_);
        while ($sql->fetch()) {
            $dados['id']    =   $id_;
            $dados['nome']  =   $nome_;
            $dados['cod']   =   $cod_;
        }
        $sql->close();
        return($dados);
    }

    // Edita a categoria escolhida
    public function EditaCategoria($id, $nome, $cod)
    {
        $sql = $this->conexao->prepare("UPDATE categoria SET nome=?, cod=? WHERE id=?");
        $sql->bind_param('ssd', $nome, $cod, $id);
        if ($sql->execute()) {
            return(1);
        } else {
            return(0);
        }
        $sql->close();
    }

    // Apaga uma categoria:
    public function ApagaCategoria($idCat)
    {
        $sql = $this->conexao->prepare("DELETE FROM categoria WHERE id = ?");
        $sql->bind_param('d', $idCat);
        if ($sql->execute()) {
            $nomeCat = $this->GetCategoriaID($idCat);
            $this->CadLog("Exclusão da categoria -> ".$nomeCat['nome'],"Usuário realizou exclusão de uma categoria");
            return(1);
        } else {
            return(0);
        }
        $sql->close();
    }

    // Pega o id da categoria de acordo com o nome
    public function GetIdCategoria($nome){
        $sql = $this->conexao->prepare("SELECT id FROM categoria WHERE nome = ?");
        $sql->bind_param('s',$nome);
        $sql->execute();
        $sql->bind_result($id);
        $sql->fetch();
        $sql->close();
        return($id);
    }
}

