<?php
$prod = $Produto->GetProdutos();
for ( $i=0; $i < count($prod['id']); $i++) {
  $prodCategoria[$prod['id'][$i]]  = $Catalogar->GetCategoriaProd($prod['id'][$i]);
}
$x=0;
?>
<!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Dashboard</h1>
    </div>
    <div class="infor">
      <a href="?page=cadastraProduto" class="btn-action">Adicionar novo produto</a>
    </div>
    <?php 
      for ( $i=0; $i < count($prod['id']); $i++) { 
        $x++;
    ?>
      <?php if ($i == 0) { ?>
          <ul class="product-list">
      <?php } ?>
          <li>
            <div class="product-image">
              <img src="assets/images/product/<?=$prod['imagem'][$i];?>" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />
            </div>
            <div class="product-info">
              <div class="product-name"><span><?=$prod['nome'][$i];?></span></div>
              <div class="product-price"><span class="special-price"><?=$prod['qnt'][$i];?> Disponiveis</span> <span>R$<?=$prod['preco'][$i];?></span></div>
            </div>
          </li>
      <?php 
        if ($x %4 == 0) {
          echo "</ul> <ul class='product-list'>"; 
        }
      ?>
      
    <?php } ?>
  </main>
  <!-- Main Content -->
