<?php
$cat = $Categoria->GetCategoria();
?>
<script>
function ExcluiCategoria(idCat){
  var resp = confirm("Tem certeza que deseja apagar essa categoria ?");
  if(resp){
    location.href = "config/apagaCategoria.php?id="+idCat;
  }
}
</script>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Categories</h1>
      <a href="?page=cadastraCategoria" class="btn-action">Adicionar nova Categoria</a>
    </div>
      <table  class="data-grid"  id="table_id" class="display">
        <thead>
          <tr class="data-row">
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Nome</span>
            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Código</span>
            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Ações</span>
            </th>
          </tr>
        </thead>
        <tbody>
        <?php for ( $i = 0; $i < count($cat['id']); $i++) { ?>
          <tr class="data-row">
            <td class="data-grid-td">
              <span class="data-grid-cell-content"><?=$cat['nome'][$i];?></span>
            </td>
            <td class="data-grid-td">
              <span class="data-grid-cell-content"><?=$cat['cod'][$i]; ?></span>
            </td>
            <td class="data-grid-td">
              <div class="actions">
                <div class="action edit">
                  <span>
                    <a href="?page=cadastraCategoria&id=<?=$cat['id'][$i];?>">
                      Editar
                    </a>
                  </span>
                </div>
                <div class="action delete">
                  <span>
                  <a href="javascript:ExcluiCategoria(<?=$cat['id'][$i];?>)">
                    Deletar
                  </a>
                  </span>
                </div>
              </div>
            </td>
          </tr>
        <?php } ?>
        </tbody>
    </table>
  </main>
  <!-- Main Content -->